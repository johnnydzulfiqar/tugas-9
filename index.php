<?php
require_once("animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name;
echo "<br>";
echo "Name : " . $sheep->legs;
echo "<br>";
echo "Name : " . $sheep->cold_blooded;
echo "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo "Name : " . $frog->name;
echo "<br>";
echo "Name : " . $frog->legs;
echo "<br>";
echo "Name : " . $frog->cold_blooded;
echo "<br>";
echo "Name : " . $frog->jump;
echo "<br>";
echo "<br>";

$ape = new Ape("Kera sakti");
echo "Name : " . $ape->name;
echo "<br>";
echo "Name : " . $ape->legs;
echo "<br>";
echo "Name : " . $ape->cold_blooded;
echo "<br>";
echo "Name : " . $ape->yell;
echo "<br>";
echo "<br>";
